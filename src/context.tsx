import React, {createContext, useReducer} from 'react';
import {PageReducer, CityReducer} from './reducers';

type PageType = {
  isFirstPageClicked: boolean;
  isSecondPageClicked: boolean;
};

type CityType = {
  city: string;
};

type InitialStateType = {
  page: PageType;
  city: CityType;
};

const pageState = {
  isFirstPageClicked: true,
  isSecondPageClicked: false,
};

const cityState = {
  city: 'Kosice',
};

const initialState = {
  page: pageState,
  city: cityState,
};

const AppContext = createContext<{
  state: InitialStateType;
  dispatch: React.Dispatch<any>;
}>({
  state: initialState,
  dispatch: () => null,
});

const MainReducer = ({page, city}: InitialStateType, action) => ({
  page: PageReducer(page, action),
  city: CityReducer(city, action),
});

const AppProvider: React.FC = ({children}) => {
  const [state, dispatch] = useReducer(MainReducer, initialState);

  return (
    <AppContext.Provider value={{state, dispatch}}>
      {children}
    </AppContext.Provider>
  );
};

export {AppContext, AppProvider, CityType, PageType};
