type Main = {
  feels_like: number;
  humidity: number;
  pressure: number;
  temp: number;
  temp_max: number;
  temp_min: number;
};

type Sys = {
  country: string;
  timezone: number;
  sunrise: number;
  sunset: number;
};

type Weather = {
  id: number;
  main: string;
  description: string;
  icon: string;
};

type WeatherList = {
  clouds: {all: number};
  coord: {lat: number; lon: number};
  dt: number;
  id: number;
  main: Main;
  name: string;
  sys: Sys;
  visibility: 7000;
  weather: [Weather];
  wind: {speed: number; deg: number};
};

type WeatherSeveralCityData = {
  cnt: number;
  list: [WeatherList];
};

export {WeatherList, WeatherSeveralCityData};
