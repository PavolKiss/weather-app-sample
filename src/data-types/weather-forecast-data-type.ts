type Main = {
  feels_like: number;
  grnd_level: number;
  humidity: number;
  pressure: number;
  sea_level: number;
  temp: number;
  temp_kf: number;
  temp_max: number;
  temp_min: number;
};

type Weather = {
  description: string;
  icon: string;
  id: number;
  main: string;
};

type Wind = {
  deg: number;
  speed: number;
};

type WeatherDataForecast = WeatherDataForecastList & {
  clouds: {
    all: number;
  };
  dt: number;
  dt_txt: string;
  main: Main;
  pop: number;
  sys: {
    pod: string;
  };
  visibility: number;
  weather: [Weather];
  wind: Wind;
};

type Coord = {lat: number; lon: number};

type City = {
  coord: Coord;
  country: string;
  id: number;
  name: string;
  population: number;
  sunrise: number;
  sunset: number;
  timezone: number;
};

type List = {
  clouds: {all: number};
  dt: number;
  dt_txt: string;
  main: Main;
  pop: number;
  sys: {pod: string};
  visibility: number;
  weather: [Weather];
  wind: Wind;
};

type WeatherDataForecastList = {
  city: City;
  cnt: number;
  cod: string;
  list: [List];
  message: number;
};

export {WeatherDataForecast, WeatherDataForecastList};
