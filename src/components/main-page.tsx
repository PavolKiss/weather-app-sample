import React, {FC, useContext} from 'react';
import {GlobalStyle, BackgroundImage, Container} from '../styles/main-page';
import {AppContext} from '../context';
import BottomContent from './bottom-content';
import SearchContent from './search-content';

const MainPage: FC = () => {
  const {state} = useContext(AppContext);

  return (
    <>
      <GlobalStyle />
      <Container isSecondPage={state.page.isSecondPageClicked}>
        {state.page.isSecondPageClicked && state.page.isSecondPageClicked ? (
          <SearchContent />
        ) : (
          <BottomContent />
        )}
      </Container>
    </>
  );
};

export default MainPage;
