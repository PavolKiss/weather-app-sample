import React, {FC, useContext, useState} from 'react';
import {useGetWeather} from './../api/get-weather';
import {WeatherList} from '../data-types/weather-serveral-city-data';
import LoadingPage from './loading-page';
import {AppContext} from '../context';
import {getCityAction, PageEnumType} from '../reducers';
import {
  StyledDiv,
  StyledInput,
  Wrapper,
  LocationWrapper,
  CityWrapper,
} from '../styles/search-content-style';

const SearchContent: FC = () => {
  const [searchTerm, setSearchTerm] = useState<string>('');
  const {dispatch} = useContext(AppContext);

  const {data, isLoading} = useGetWeather(
    'group?id=3060972,724627,724443,724144,723554'
  );

  const filteredData = data?.list?.filter((item: WeatherList) =>
    item?.name?.toLocaleLowerCase().includes(searchTerm)
  );

  return (
    <>
      {isLoading ? (
        <LoadingPage />
      ) : (
        <Wrapper>
          <StyledDiv>Location</StyledDiv>
          <StyledInput
            type="text"
            value={searchTerm}
            onChange={e => setSearchTerm(e.target.value.toLowerCase())}
            placeholder="Search city ..."
          />
          {filteredData?.map((location: WeatherList) => (
            <LocationWrapper
              key={location?.id}
              onClick={() => {
                dispatch(getCityAction(location?.name));
                dispatch({type: PageEnumType.FIRST_PAGE});
              }}
            >
              <CityWrapper isCity>{location?.name}</CityWrapper>
              <CityWrapper isTempeture>
                {Math.round(location?.main?.temp)}&deg;C
              </CityWrapper>
            </LocationWrapper>
          ))}
        </Wrapper>
      )}
    </>
  );
};

export default SearchContent;
