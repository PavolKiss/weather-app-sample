import React, {FC} from 'react';
import {Wrapper, LoadingImage} from '../styles/loading-page';
import loadIcon from '../icons/load-icon.svg';

const LoadingPage: FC = () => (
  <Wrapper>
    <LoadingImage src={`${loadIcon}`} />
  </Wrapper>
);

export default LoadingPage;
