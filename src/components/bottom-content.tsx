import React, {FC, useContext} from 'react';
import dayjs from 'dayjs';
import Card from './card';
import CloudIcon from '../icons/cloud-icon';
import LocationImage from '../icons/location-icon';
import {useGetWeather} from './../api/get-weather';
import {DATE} from '../constants';
import {weatherMockData} from '../mocks/weather-data';
import {
  WeatherDataForecast,
  WeatherDataForecastList,
} from '../data-types/weather-forecast-data-type';
import LoadingPage from './loading-page';
import {AppContext} from '../context';
import {PageEnumType} from '../reducers';
import {
  StyledDiv,
  CelziusSpan,
  TimeWrapper,
  Wrapper,
  LocationWrapper,
  Location,
  TemperatureWrapper,
  CardWrapper,
  ContentWrapper,
  BottomContentWrapper,
} from '../styles/bottom-content-style';

const getForecastWeatherForThreeDays = (data: WeatherDataForecastList) => {
  let dailyData: WeatherDataForecast[] = [];

  if (data) {
    data?.list?.map((item: WeatherDataForecast) => {
      const dateTime = new Date(item.dt * 1000);
      const day = dateTime.getDate();
      if (!dailyData[day]) dailyData[day] = {...item};
    });
    return dailyData.slice(3, 6);
  }
  return null;
};

const BottomContent: FC = () => {
  const {state, dispatch} = useContext(AppContext);

  const cityName = state.city.city;

  const {data: dataForecast, isLoading: isLoadingForecastData} = useGetWeather(
    `forecast?q=${cityName}`
  );
  const {data, isLoading} = useGetWeather(`weather?q=${cityName}`);
  const listWeatherMockData = weatherMockData(data);
  const forecastWeatherMockData = getForecastWeatherForThreeDays(dataForecast);

  const formatedDate = dayjs(DATE).format('dddd, DD MMM YYYY | h:mm A');

  return (
    <>
      {isLoading && isLoadingForecastData ? (
        <LoadingPage />
      ) : (
        <BottomContentWrapper>
          <StyledDiv>
            <TimeWrapper>{formatedDate}</TimeWrapper>
            <LocationWrapper
              onClick={() => dispatch({type: PageEnumType.SECOND_PAGE})}
            >
              <div>{data?.name}, Slovakia</div>
              <Location>
                <LocationImage />
              </Location>
            </LocationWrapper>
          </StyledDiv>

          <ContentWrapper>
            <div>
              <CloudIcon />
              <div>{data?.weather.map(item => item.main)}</div>
            </div>
            <Wrapper>
              <h1>{Math.round(data?.main?.temp)}</h1>
              <CelziusSpan>&deg;C</CelziusSpan>
            </Wrapper>
            <div>
              <TemperatureWrapper>
                {Math.round(data?.main?.temp_max)}&deg;C &uarr;
              </TemperatureWrapper>
              <TemperatureWrapper>
                {Math.round(data?.main?.temp_min)}&deg;C &darr;
              </TemperatureWrapper>
            </div>
          </ContentWrapper>

          <CardWrapper>
            {listWeatherMockData?.map(item => (
              <Card
                key={item?.id}
                image={item?.icon}
                title={item?.title}
                description={item?.description}
              />
            ))}
            {forecastWeatherMockData?.map(item => {
              const title = dayjs(item?.dt_txt).format('ddd, DD');
              const max_temp = Math.round(item?.main?.temp_max);
              const min_temp = Math.round(item?.main?.temp_min);
              return item?.weather?.map(item => (
                <Card
                  key={item?.id}
                  title={title}
                  imgSrc={item?.icon}
                  max_temp={max_temp}
                  min_temp={min_temp}
                  isBordered
                />
              ));
            })}
          </CardWrapper>
        </BottomContentWrapper>
      )}
    </>
  );
};

export default BottomContent;
