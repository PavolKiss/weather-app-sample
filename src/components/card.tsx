import React, {FC} from 'react';
import {StyledDiv, Wrapper, ContentWrapper} from '../styles/card-style';

type CardProps = {
  image?: JSX.Element;
  title: string | number;
  description?: string | number;
  isBordered?: boolean;
  imgSrc?: string;
  max_temp?: number;
  min_temp?: number;
};

const Card: FC<CardProps> = ({
  image,
  imgSrc,
  title,
  description,
  max_temp,
  min_temp,
  isBordered,
}) => (
  <StyledDiv isBordered={isBordered}>
    <Wrapper>
      {image ? (
        image
      ) : (
        <img
          src={`http://openweathermap.org/img/w/${imgSrc}.png`}
          alt={`${title}`}
        />
      )}

      <ContentWrapper isTitle>{title}</ContentWrapper>
      <ContentWrapper>
        {description ? (
          description
        ) : (
          <>
            {max_temp}&deg;C &uarr; {min_temp}&deg;C &darr;
          </>
        )}
      </ContentWrapper>
    </Wrapper>
  </StyledDiv>
);

export default Card;
