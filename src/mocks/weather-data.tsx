import React from 'react';
import dayjs from 'dayjs';
import {WeatherData} from '../data-types/weather-data-type';
import HumidityIcon from '../icons/humidity-icon';
import BarometerIcon from '../icons/barometer-icon';
import WindIcon from '../icons/wind-icon';
import SunriseIcon from '../icons/sunrise-icon';
import SunsetIcon from '../icons/sunset-icon';
import DayTimeIcon from '../icons/daytime-icon';

const convertMpsToKmph = (mps: number) => {
  return `${Math.round(mps * 3.6)} km/h`;
};

export const weatherMockData = (data: WeatherData) => [
  {
    id: 1,
    icon: <HumidityIcon />,
    title: `${data?.main?.humidity}%`,
    description: 'Humidity',
  },
  {
    id: 2,
    icon: <BarometerIcon />,
    title: `${data?.main?.pressure.toLocaleString('en')}mBar`,
    description: 'Pressure',
  },
  {
    id: 3,
    icon: <WindIcon />,
    title: convertMpsToKmph(data?.wind?.speed),
    description: 'Wind',
  },
  {
    id: 4,
    icon: <SunriseIcon />,
    title: dayjs.unix(data?.sys?.sunrise).format('h:mm A'),
    description: 'Sunrise',
  },
  {
    id: 5,
    icon: <SunsetIcon />,
    title: dayjs.unix(data?.sys?.sunset).format('h:mm A'),
    description: 'Sunset',
  },
  {
    id: 6,
    icon: <DayTimeIcon />,
    title: dayjs.unix(data?.dt).format('HH m'),
    description: 'Daytime',
  },
];
