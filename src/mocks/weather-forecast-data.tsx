/*
 ** This is just an example for 'mock data' using ICONS from Figma
 ** https://www.figma.com/file/0m8FSeOhwjVHDxAWWeV31P/Minimal-Weather-App?node-id=21%3A0
 */

import React from 'react';
import dayjs from 'dayjs';
import {WeatherDataForecast} from '../data-types/weather-forecast-data-type';
import SunIcon from '../icons/sun-icon';
import CloudyIcon from '../icons/cloudy-icon';
import HazyIcon from '../icons/hazy-icon';

const weatherForecastMockData = (data: WeatherDataForecast[]) => {
  const title = data?.map(item => item.dt_txt);

  if (data) {
    return [
      {
        id: 1,
        icon: <SunIcon />,
        title: dayjs(title[0]).format('ddd, DD'),
        description: 'Description',
      },
      {
        id: 2,
        icon: <CloudyIcon />,
        title: dayjs(title[1]).format('ddd, DD'),
        description: 'Description',
      },
      {
        id: 3,
        icon: <HazyIcon />,
        title: dayjs(title[2]).format('ddd, DD'),
        description: 'Description',
      },
    ];
  }
  return null;
};
