import styled, {createGlobalStyle} from 'styled-components';
import {MEDIA} from './media-queries';
import backgroundImage from '../icons/background-icon.svg';

const GlobalStyle = createGlobalStyle`
  * {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    font-size: 16px;
    font-family: "Barlow", sans-serif;

    ${MEDIA.tablet} {
      font-size: 18px;
    }

    ${MEDIA.desktop} {
      font-size: 20px;
    }
  }

  body {
  background-image: url(${backgroundImage});
  background-position: top left;
  background-repeat: no-repeat;
  background-size: cover;
  height: 100%;
}

  h1 {
    font-size: 4rem;
    font-weight: 300;
  }

  h2 {
    font-size: 1.1rem;
    font-weight: 500;
  }
`;

const BackgroundImage = styled.img`
  width: 100%;
  height: auto;
  position: absolute;
  left: 0px;
  top: 0px;
  z-index: -1;
`;

type ContainerProps = {
  isSecondPage?: boolean;
};

const Container = styled.div<ContainerProps>`
  background: #fff;
  border-top-left-radius: 1rem;
  border-top-right-radius: 1rem;
  box-shadow: 0px -18px 20px 0px rgb(119 119 119 / 47%);

  ${({isSecondPage}) =>
    isSecondPage && 'transition: all 0.5s ease; margin-top: 2rem;'}

  ${MEDIA.tablet} {
    box-shadow: none;
    /* margin-top: 26.5rem; */
  }

  ${MEDIA.desktop} {
    box-shadow: none;
    /* margin-top: 17rem; */
  }
`;

export {GlobalStyle, BackgroundImage, Container};
