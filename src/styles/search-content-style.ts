import styled from 'styled-components';
import {COLORS} from '../constants';
import {MEDIA} from './media-queries';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: 90vh;

  ${MEDIA.desktop} {
    height: 95.5vh;
  }
`;

const StyledDiv = styled.div`
  margin-top: 1.5rem;
  font-weight: 500;
  color: ${COLORS.lightGray};
`;

const StyledInput = styled.input`
  min-width: 90%;
  border: none;
  text-indent: 1rem;
  background-color: ${COLORS.inputLightGray};
  color: ${COLORS.black};
  font-weight: 500;
  font-size: 1.12rem;
  letter-spacing: -1px;
  border-radius: 5px;
  margin: 1.5rem 0;
  padding: 0.5rem 0;
  &::placeholder {
    color: ${COLORS.inputGray};
    font-style: italic;
  }

  ${MEDIA.desktop} {
    min-width: 50%;
  }
`;

const LocationWrapper = styled.div`
  display: flex;
  margin-top: 0.5rem;
  justify-content: space-between;
  min-width: 90%;
  font-size: 1.12rem;
  cursor: pointer;
  border: 2px solid transparent;

  ${MEDIA.desktop} {
    min-width: 50%;
    &:hover {
      border: 2px solid ${COLORS.blue};
      transition: all 0.4s ease;
    }
  }
`;

type LocationWrapperProps = {
  isCity?: boolean;
  isTempeture?: boolean;
};

const CityWrapper = styled.div<LocationWrapperProps>`
  ${({isTempeture, isCity}) => {
    switch (true) {
      case isTempeture:
        return `font-weight: 300; color: ${COLORS.lightGray};`;
      case isCity:
        return `color: ${COLORS.gray}; font-size: 1.12rem; letter-spacing: -1px;`;
    }
  }}
`;

export {StyledDiv, StyledInput, Wrapper, LocationWrapper, CityWrapper};
