import styled from 'styled-components';
import {COLORS} from '../constants';
import {MEDIA} from './media-queries';

type StyledDivProps = {
  isBordered?: boolean;
};

const StyledDiv = styled.div<StyledDivProps>`
  flex: 1 0 30%;
  display: flex;
  justify-content: center;
  margin: 0.5rem 0.35rem;
  padding: 1rem;
  border-radius: 1rem;

  ${MEDIA.tablet} {
    box-shadow: none;
  }

  ${({isBordered}) =>
    isBordered && 'box-shadow: 0px 5px 16px -8px rgba(0,0,0,0.3)'}
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

type ContentWrapperProps = {
  isTitle?: boolean;
};

const ContentWrapper = styled.div<ContentWrapperProps>`
  font-weight: 500;
  text-align: center;
  font-size: ${({isTitle}) => (isTitle ? '1rem' : '0.5rem')};
  color: ${({isTitle}) => (isTitle ? `${COLORS.gray}` : `${COLORS.lightGray}`)};
  &:not(:last-child) {
    margin-top: 0.5rem;
  }
`;

export {StyledDiv, Wrapper, ContentWrapper};
