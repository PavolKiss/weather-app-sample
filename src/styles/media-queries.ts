const customMediaQuery = (minWidth: number) =>
  `@media (min-width: ${minWidth}px)`;

export const MEDIA = {
  desktop: customMediaQuery(922),
  tablet: customMediaQuery(768),
};
