import styled from 'styled-components';
import {COLORS} from '../constants';
import {MEDIA} from './media-queries';

const BottomContentWrapper = styled.div`
  height: 100%;
`;

const StyledDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 18rem;

  ${MEDIA.tablet} {
    margin-top: 16rem;
  }

  ${MEDIA.desktop} {
    margin-top: 17rem;
    justify-content: space-around;
  }
`;

const ContentWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin-top: 2rem;
  > :first-child {
    display: flex;
    align-items: center;
    flex-direction: column;
  }
`;

const CelziusSpan = styled.span`
  font-size: 1.5rem;
  color: ${COLORS.gray};
  font-weight: 500;
  margin-top: 0.9rem;
`;

const TimeWrapper = styled.div`
  font-size: 0.87rem;
  color: ${COLORS.lightGray};
  margin-left: 0.5rem;
`;

const LocationWrapper = styled.div`
  color: ${COLORS.blue};
  font-weight: 500;
  display: flex;
  align-items: center;
  cursor: pointer;
  background: rgba(13, 159, 234, 0.08);
  padding: 1rem 1rem;
  border-bottom-left-radius: 2rem;

  ${MEDIA.desktop} {
    background: none;
  }
`;

const Location = styled.div`
  margin-left: 0.3rem;
`;

const TemperatureWrapper = styled.div`
  color: ${COLORS.lightGray};
  font-weight: 300;
  &:not(:last-child) {
    margin-bottom: 0.5rem;
  }
`;

const Wrapper = styled.div`
  display: flex;
`;

const CardWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export {
  StyledDiv,
  CelziusSpan,
  TimeWrapper,
  Wrapper,
  LocationWrapper,
  Location,
  TemperatureWrapper,
  CardWrapper,
  ContentWrapper,
  BottomContentWrapper,
};
