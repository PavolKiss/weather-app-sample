import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const LoadingImage = styled.img`
  max-width: 15%;
  margin-top: 15rem;
  z-index: 2;
`;

export {Wrapper, LoadingImage};
