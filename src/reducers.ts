import {PageType, CityType} from './context';

enum PageEnumType {
  FIRST_PAGE = 'FIRST_PAGE',
  SECOND_PAGE = 'SECOND_PAGE',
}

const PageReducer = (state: PageType, action) => {
  switch (action.type) {
    case PageEnumType.FIRST_PAGE:
      return {
        ...state,
        isFirstPageClicked: true,
        isSecondPageClicked: false,
      };
    case PageEnumType.SECOND_PAGE:
      return {
        ...state,
        isSecondPageClicked: true,
        isFirstPageClicked: false,
      };
    default:
      return state;
  }
};

enum CityEnumType {
  GET_CITY = 'GET_CITY',
}

const getCityAction = (city?: string) => {
  return {type: CityEnumType.GET_CITY, city};
};

const CityReducer = (state: CityType, action) => {
  switch (action.type) {
    case CityEnumType.GET_CITY:
      return {
        ...state,
        city: action.city,
      };
    default:
      return state;
  }
};

export {PageEnumType, PageReducer, getCityAction, CityReducer};
