import React from 'react';
import {API_KEY} from '../constants';
import {WeatherData} from './../data-types/weather-data-type';

export const useGetWeather = (endpoint: string) => {
  const [isLoading, setIsLoading] = React.useState<boolean>(true);
  const [data, setData] = React.useState<WeatherData>(null);

  React.useEffect(() => {
    (async () => {
      try {
        const response = await fetch(
          `https://api.openweathermap.org/data/2.5/${endpoint}&units=metric&appid=${API_KEY}`
        );
        const responseData = await response.json();
        setData(responseData);
        response.status === 200 && setIsLoading(false);
      } catch (error) {
        console.log(error);
      }
    })();
  }, [endpoint]);

  return {isLoading, data};
};
