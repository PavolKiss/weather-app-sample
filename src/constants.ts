const API_KEY: string = process.env.WEATHER_API_KEY || '';

const DATE = new Date();

const COLORS = {
  black: '#000000',
  gray: '#444444',
  lightGray: '#999999',
  inputLightGray: '#F3F3F3;',
  inputGray: '#9F9F9F',
  blue: '#0DA0EA',
};

export {DATE, COLORS, API_KEY};
