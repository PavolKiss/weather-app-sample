import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import MainPage from './components/main-page';
import {AppProvider} from './context';

const App: React.FC = () => (
  <AppProvider>
    <MainPage />
  </AppProvider>
);

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);
