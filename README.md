# Weather application

It is a basic weather application which gives you current weather conditions based on your location.

Currently only few Slovak countries are available: [Bratislava, Humenne, ~~Koromla~~, Kosice, Michalovce, Sobrance]

## Installation

Use the package manager [yarn](https://yarnpkg.com/getting-started/install) to install weather application.

```bash
yarn install
```

## Before running application

:heavy_exclamation_mark: Before running application, please make sure you have already created **.env** file in the root of your application. Do not forget to put your **WEATHER_API_KEY** into this file :heavy_exclamation_mark:

root  
 ├─ src  
 ├─ ...  
 ├─ ...  
 ├─ **.env**

## Run application

To run application type into the command line:

```bash
yarn dev
```

- Application will automatically show up in your browser.
